//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Enrollees.EFModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class Предметы_для_специальностей
    {
        public byte код_предмета { get; set; }
        public int код_специальности { get; set; }
    
        public virtual Предметы Предметы { get; set; }
        public virtual Специальности Специальности { get; set; }
    }
}
