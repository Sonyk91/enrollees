﻿using System.Windows.Forms;

namespace Enrollees
{
    public partial class PasswordRequestForm : Form
    {
        public PasswordRequestForm()
        {
            InitializeComponent();
        }

        private void sendPassword_MouseUp(object sender, MouseEventArgs e)
        {
            if(password.Text != null)
            {
                Program.Password = password.Text;
                Close();
            }
            else
            {
                MessageBox.Show("Пароль не должен быть пустым", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
