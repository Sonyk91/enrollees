﻿#region Usings

using Enrollees.EFModels;
using Enrollees.View.Domains;

#endregion

namespace Enrollees.View.Converters
{
    public static class ConvertПредметToDomainSubject
    {
        public static Subject ToDomain(Предметы subject)
        {
            return new Subject
                   {
                       Код_предмета = subject.Код_предмета,
                       Название_предмета = subject.Название_предмета
                   };
        }

        public static Предметы ToViewModel(Subject subject)
        {
            return new Предметы
                   {
                       Код_предмета = subject.Код_предмета,
                       Название_предмета = subject.Название_предмета
                   };
        }
    }
}