﻿#region Usings

using Enrollees.EFModels;
using Enrollees.View.Domains;

#endregion

namespace Enrollees.View.Converters
{
    public static class ConvertСпециальностиToDomainSpeciality
    {
        public static Speciality ToDomain(Специальности speciality)
        {
            return new Speciality
                   {
                       Код_специальности = speciality.Код_специальности,
                       Название_специальности = speciality.Название_специальности,
                       Назание_факультета = speciality.Назание_факультета
                   };
        }

        public static Специальности ToViewModel(Speciality speciality)
        {
            return new Специальности
                   {
                       Код_специальности = speciality.Код_специальности,
                       Название_специальности = speciality.Название_специальности,
                       Назание_факультета = speciality.Назание_факультета
                   };
        }
    }
}