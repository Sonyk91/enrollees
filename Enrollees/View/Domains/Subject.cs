﻿using System;

namespace Enrollees.View.Domains
{
    public class Subject
    {
        public byte Код_предмета { get; set; }
        public string Название_предмета { get; set; }

        public override string ToString()
        {
            return string.Format("{0}", Название_предмета);
        }
    }
}