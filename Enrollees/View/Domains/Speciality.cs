﻿namespace Enrollees.View.Domains
{
    public class Speciality
    {
        public int Код_специальности { get; set; }
        public string Название_специальности { get; set; }
        public string Назание_факультета { get; set; }

        public override string ToString()
        {
            return Название_специальности + " (" + Назание_факультета + ")";
        }
    }
}