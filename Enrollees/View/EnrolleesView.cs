﻿#region Usings

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Enrollees.EFModels;
using Enrollees.Properties;
using Enrollees.View.Converters;
using Enrollees.View.Dialogs;
using Enrollees.View.Domains;

#endregion

namespace Enrollees.View
{
    public partial class EnrolleesView : Form
    {
        /// <summary>
        /// Выбранная вкладка
        /// </summary>
        private int currentTabIndex;
        /// <summary>
        /// Объект для работы с базой данных
        /// </summary>
        private readonly PKEntities dataBase;

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public EnrolleesView()
        {
            InitializeComponent();
            // Подключаемся к базе данных
            dataBase = PKEntitiesFactory.Instance;
        }

        /// <summary>
        /// Загрузка данных в зависимости от выбранной вкладки
        /// </summary>
        private void tabControl1_Selected(object sender, TabControlEventArgs e)
        {
            currentTabIndex = e.TabPageIndex;
            switch (currentTabIndex)
            {
                case 0:
                    LoadWorksheets(load: true);
                    break;
                case 1:
                    LoadSubjects();
                    break;
                case 2:
                    LoadSpecialities();
                    break;
                case 3:
                    LoadGroupFormation();
                    break;
            }
        }

        /// <summary>
        /// Загрузка данных для вкладки формирования групп
        /// </summary>
        private void LoadGroupFormation()
        {
            comboBoxSubject.Items.Clear();
            // Загружаем предметы в выпадающий список
            var subjects = dataBase.Предметы.ToList();
            comboBoxSubject.Items.AddRange(subjects.ConvertAll(ConvertПредметToDomainSubject.ToDomain).ToArray());
        }

        /// <summary>
        /// Загрузка данных для вкладки Специальности
        /// </summary>
        private void LoadSpecialities()
        {
            specialityDataGridView.Rows.Clear();
            foreach (var s in dataBase.Специальности)
            {
                specialityDataGridView.Rows.Add(s.Код_специальности, s.Название_специальности, s.Назание_факультета);
            }
        }

        /// <summary>
        /// Загрузка данных для вкладки Предметы
        /// </summary>
        private void LoadSubjects()
        {
            subjectsDataGridView.Rows.Clear();
            foreach (var s in dataBase.Предметы)
            {
                subjectsDataGridView.Rows.Add(s.Код_предмета, s.Название_предмета);
            }

            var idSelectedSubject = GetIdSelectedInDataGrid(subjectsDataGridView);
            LoadDataSubjectInEditFields((byte) idSelectedSubject);
        }

        /// <summary>
        /// Загрузка данных для вкладки Анктеы абитуриентов
        /// </summary>
        private void LoadWorksheets(bool update = false, bool load = false)
        {
            // Загрузить данные во вкладку Абитуриенты.
            var selectedRow = GetSelectedRowInDataGridView(worksheetDataGridView);

            worksheetDataGridView.Rows.Clear();
            foreach (var worksheet in dataBase.Анкета_абитуриента)
            {
                worksheetDataGridView.Rows.Add(worksheet.Код_абитуриента, worksheet.Дата_регистрации, worksheet.Фамилия,
                                               worksheet.Имя, worksheet.Отчество,
                                               worksheet.Пол ? "М" : "Ж",
                                               worksheet.Дата_рождения, "Открыть");
            }

            var enabledGroupButtons = worksheetDataGridView.RowCount != 0;
            buttonUpdateSelectEnrollee.Enabled = buttonDeleteSelectedEnrollee.Enabled = enabledGroupButtons;

            if (worksheetDataGridView.Rows.Count != 0)
            {
                if (load)
                {
                    worksheetDataGridView.Rows[0].Selected = true;
                    LoadDataEnrolleeInEditFields(selectedRowIndex: 0);
                }
                else
                {
                    if (update)
                    {
                        worksheetDataGridView.Rows[selectedRow].Selected = true;
                    }
                    else
                    {
                        selectedRow = worksheetDataGridView.RowCount - 1;
                        worksheetDataGridView.Rows[selectedRow].Selected = true;
                        LoadDataEnrolleeInEditFields(selectedRow);
                    }
                }
            }
        }

        /// <summary>
        /// Загрузка формы при первом открытии
        /// </summary>
        private void Form1_Load(object sender, EventArgs e)
        {
            LoadWorksheets(load: true);
        }

        #region Вкладка анкеты абитуриентов

        /// <summary>
        /// Событие возникает при клике на ячейку. При клике выделяется строка и обновляются данные в редактируемых полях
        /// </summary>
        private void worksheetDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            LoadDataEnrolleeInEditFields(e.RowIndex);

            switch (worksheetDataGridView.Columns[e.ColumnIndex].Name)
            {
                case "personalData":
                    var insertOrUpdatePersonalDataForm = new InsertOrUpdatePersonalDataForm();
                    insertOrUpdatePersonalDataForm.ПерсональныеДанные = new Персональные_данные
                                                                        {
                                                                            Код_абитуриента = (int) worksheetDataGridView.Rows[e.RowIndex].Cells[0].Value
                                                                        };
                    insertOrUpdatePersonalDataForm.ShowDialog();
                    break;
            }
        }

        /// <summary>
        /// Загрузка данных об абитуриентах
        /// </summary>
        /// <param name="selectedRowIndex">Выделенный абитуриент в гриде</param>
        private void LoadDataEnrolleeInEditFields(int selectedRowIndex)
        {
            if (selectedRowIndex == -1)
            {
                return;
            }

            var idEnrollee = (int) worksheetDataGridView.Rows[selectedRowIndex].Cells[0].Value;
            if (idEnrollee > 0)
            {
                // Загрузить данные анкеты.
                var абитуриент = dataBase.Анкета_абитуриента.FirstOrDefault(a => a.Код_абитуриента == idEnrollee);

                if (абитуриент != null)
                {
                    tbFamily.Text = абитуриент.Фамилия;
                    tbName.Text = абитуриент.Имя;
                    tbLastName.Text = абитуриент.Отчество;
                    датаРегистрации.Value = абитуриент.Дата_регистрации;
                    датаРождения.Value = абитуриент.Дата_рождения;
                    if (абитуриент.Пол)
                    {
                        полМ.Checked = true;
                        полЖ.Checked = false;
                    }
                    else
                    {
                        полМ.Checked = false;
                        полЖ.Checked = true;
                    }
                }
            }
        }

        /// <summary>
        /// Событие возникает при нажатии на кнопку добавить
        /// </summary>
        private void buttonAddEnrollee_Click(object sender, EventArgs e)
        {
            InsertOrUpdateEnrollee();
        }

        /// <summary>
        /// Событие возникает при нажатии на кнопку Обновить
        /// </summary>
        private void buttonUpdateSelectedEnrollee_Click(object sender, EventArgs e)
        {
            InsertOrUpdateEnrollee(update: true);
        }

        /// <summary>
        /// Событие возникает при нажатии на кнопку Удалить
        /// </summary>
        private void buttonDeleteSelectedEnrollee_Click(object sender, EventArgs e)
        {
            var firstSelectedRow = GetSelectedRowInDataGridView(worksheetDataGridView);
            var idSelectedEnrolle = GetIdSelectedInDataGrid(worksheetDataGridView, firstSelectedRow);

            if (idSelectedEnrolle != -1)
            {
                var enrollee = dataBase.Анкета_абитуриента.FirstOrDefault(a => a.Код_абитуриента == idSelectedEnrolle);

                if (enrollee != null)
                {
                    dataBase.Анкета_абитуриента.Remove(enrollee);
                }
                else
                {
                    MessageBox.Show(Resources.MessageText_EnrolleeNotFoundInDb);
                }
                SaveChanges();
                LoadWorksheets();
            }
            else
            {
                MessageBox.Show(Resources.MessageText_SelectedEnrolleForRemoved);
            }
        }

        /// <summary>
        /// Добавление или обновление карточек абитуриентов
        /// </summary>
        private void InsertOrUpdateEnrollee(bool update = false)
        {
            if (string.IsNullOrEmpty(tbFamily.Text)
                || string.IsNullOrEmpty(tbName.Text)
                || string.IsNullOrEmpty(tbLastName.Text))
            {
                MessageBox.Show(Resources.MessageText_FillInAllFields);
                return;
            }

            var анкетаАбитуриента = new Анкета_абитуриента();
            анкетаАбитуриента.Фамилия = tbFamily.Text;
            анкетаАбитуриента.Имя = tbName.Text;
            анкетаАбитуриента.Отчество = tbLastName.Text;
            анкетаАбитуриента.Дата_регистрации = датаРегистрации.Value;
            анкетаАбитуриента.Дата_рождения = датаРождения.Value;
            анкетаАбитуриента.Пол = полМ.Checked;

            ICollection<ValidationResult> errors = new List<ValidationResult>();
            if (DataAnnotationsValidator.TryValidate(анкетаАбитуриента, out errors))
            {
                var idEnrollee = update ? GetIdSelectedInDataGrid(worksheetDataGridView) : анкетаАбитуриента.Код_абитуриента;
                var анкета = dataBase.Анкета_абитуриента.FirstOrDefault(a => a.Код_абитуриента == idEnrollee);

                // Сохранить в БД.
                if (анкета != null)
                {
                    анкета.Фамилия = анкетаАбитуриента.Фамилия;
                    анкета.Имя = анкетаАбитуриента.Имя;
                    анкета.Отчество = анкетаАбитуриента.Отчество;
                    анкета.Дата_регистрации = анкетаАбитуриента.Дата_регистрации;
                    анкета.Дата_рождения = анкетаАбитуриента.Дата_рождения;
                    анкета.Пол = анкетаАбитуриента.Пол;
                }
                else
                {
                    dataBase.Анкета_абитуриента.Add(анкетаАбитуриента);
                }

                SaveChanges();
                LoadWorksheets(update);
            }
            else
            {
                MessageBox.Show(errors.First().ErrorMessage, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region Вкладка предметы

        /// <summary>
        /// Событие возникает при нажатии на кнопку добавить
        /// </summary>
        private void buttonAddSubject_Click(object sender, EventArgs e)
        {
            var addNewSubjectForm = new InsertOrUpdateSubjectForm();
            addNewSubjectForm.ShowDialog();
            LoadSubjects();
        }

        /// <summary>
        /// Событие возникает при нажатии на кнопку Обновить
        /// </summary>
        private void buttonUpdateSelectedSubject_Click(object sender, EventArgs e)
        {
            var idSelectedSubject = (byte) GetIdSelectedInDataGrid(subjectsDataGridView);
            var предмет = new Предметы();
            предмет.Название_предмета = subjectName.Text;
            предмет.Код_предмета = idSelectedSubject;

            ICollection<ValidationResult> errors = new List<ValidationResult>();

            if (DataAnnotationsValidator.TryValidate(предмет, out errors))
            {
                var обновляемыйПредмет = dataBase.Предметы.FirstOrDefault(a => a.Код_предмета == idSelectedSubject);
                if (обновляемыйПредмет != null)
                {
                    обновляемыйПредмет.Название_предмета = предмет.Название_предмета;
                    обновляемыйПредмет.Код_предмета = предмет.Код_предмета;
                    SaveChanges();
                    LoadSubjects();
                }
            }
            else
            {
                MessageBox.Show(errors.First().ErrorMessage, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Событие возникает при нажатии на кнопку Удалить
        /// </summary>
        private void buttonDeleteSelectedSubject_Click(object sender, EventArgs e)
        {
            var idSelectedSubject = (byte) GetIdSelectedInDataGrid(subjectsDataGridView);

            if (idSelectedSubject != -1)
            {
                var удаляемыйПредмет = dataBase.Предметы.FirstOrDefault(a => a.Код_предмета == idSelectedSubject);

                if (удаляемыйПредмет != null)
                {
                    dataBase.Предметы.Remove(удаляемыйПредмет);
                }
                else
                {
                    MessageBox.Show("Предмет не найден в БД");
                }
                SaveChanges();
                LoadSubjects();
            }
            else
            {
                MessageBox.Show("Выберите предмет для удаления");
            }
        }

        /// <summary>
        /// Событие возникает при клике на ячейку. При клике выделяется строка и обновляются данные в редактируемых полях
        /// </summary>
        private void subjectsDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }

            var idSelectedSubject = (byte) subjectsDataGridView.Rows[e.RowIndex].Cells[0].Value;
            LoadDataSubjectInEditFields(idSelectedSubject);
        }

        /// <summary>
        /// Загрузка данных о предмете в редактируемые поля
        /// </summary>
        /// <param name="idSelectedSubject">Выделенные предмет в гриде</param>
        private void LoadDataSubjectInEditFields(byte idSelectedSubject)
        {
            if (idSelectedSubject == -1)
            {
                return;
            }

            var предмет = dataBase.Предметы.FirstOrDefault(p => p.Код_предмета == idSelectedSubject);
            if (предмет != null)
            {
                subjectName.Text = предмет.Название_предмета;

                var кодыСпециальностейПредмета = dataBase.Предметы_для_специальностей.Where(_ => _.код_предмета == предмет.Код_предмета).ToList();

                specialityCheckedListBox.Items.Clear();
                foreach (var кодСпециальностиПредмета in кодыСпециальностейПредмета)
                {
                    var speciality = ConvertСпециальностиToDomainSpeciality
                        .ToDomain(dataBase.Специальности.First(_ => _.Код_специальности == кодСпециальностиПредмета.код_специальности));
                    specialityCheckedListBox.Items.Add(speciality, true);
                }
            }
        }

        #endregion

        #region Вкладка специальности

        /// <summary>
        /// Событие возникает при нажатии на кнопку добавить
        /// </summary>
        private void buttonAddSpeciality_Click(object sender, EventArgs e)
        {
            InserOrUpdateSpeciality();
        }

        /// <summary>
        /// Добавление или обновление специальностей
        /// </summary>
        private void InserOrUpdateSpeciality(bool update = false)
        {
            var Специальность = new Специальности();

            Специальность.Назание_факультета = facultyName.Text;
            Специальность.Название_специальности = specialityName.Text;

            ICollection<ValidationResult> errors = new List<ValidationResult>();
            if (DataAnnotationsValidator.TryValidate(Специальность, out errors))
            {
                var idSpeciality = update ? GetIdSelectedInDataGrid(specialityDataGridView) : Специальность.Код_специальности;
                var специальность = dataBase.Специальности.FirstOrDefault(s => s.Код_специальности == idSpeciality);

                if (специальность != null)
                {
                    специальность.Название_специальности = Специальность.Название_специальности;
                    специальность.Назание_факультета = Специальность.Назание_факультета;
                }
                else
                {
                    dataBase.Специальности.Add(Специальность);
                }

                SaveChanges();
                LoadSpecialities();
            }
            else
            {
                // Вывод ошибки валидации.
                MessageBox.Show(errors.First().ErrorMessage, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        
        /// <summary>
        /// Событие возникает при нажатии на кнопку Обновить
        /// </summary>
        private void buttonUpdateSelectedSpeciality_Click(object sender, EventArgs e)
        {
            InserOrUpdateSpeciality(update: true);
        }

        /// <summary>
        /// Событие возникает при клике на ячейку. При клике выделяется строка и обновляются данные в редактируемых полях
        /// </summary>
        private void specialityDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }

            var idSpeciality = (int) specialityDataGridView.Rows[e.RowIndex].Cells[0].Value;
            var специальность = dataBase.Специальности.FirstOrDefault(s => s.Код_специальности == idSpeciality);
            if (специальность != null)
            {
                specialityName.Text = специальность.Название_специальности;
                facultyName.Text = специальность.Назание_факультета;
            }
        }

        /// <summary>
        /// Событие возникает при нажатии на кнопку Удалить
        /// </summary>
        private void buttonDeleteSelectedSpeciality_Click(object sender, EventArgs e)
        {
            var firstSelectedRow = GetSelectedRowInDataGridView(specialityDataGridView);
            var idSelectedSpeciality = GetIdSelectedInDataGrid(specialityDataGridView, firstSelectedRow);

            if (idSelectedSpeciality != -1)
            {
                var speciality = dataBase.Специальности.FirstOrDefault(a => a.Код_специальности == idSelectedSpeciality);

                if (speciality != null)
                {
                    dataBase.Специальности.Remove(speciality);
                }
                else
                {
                    MessageBox.Show("Специальность не найдена");
                }
                SaveChanges();
                LoadSpecialities();
            }
            else
            {
                MessageBox.Show("Выберите специальность для удаления");
            }
        }

        #endregion

        /// <summary>
        /// Событие происходит при нажатии на пункт меню Файл "Загрузить оценки по предмету"
        /// </summary>
        private void загрузитьОценкиПоПредметуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = false;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                var subjectName = openFileDialog.SafeFileName;
                var balls = new List<Баллы>();
                using (var streamReader = new StreamReader(openFileDialog.FileName))
                {
                    while (streamReader.Peek() >= 0)
                    {
                        var strLine = streamReader.ReadLine();
                        try
                        {
                            var id = Convert.ToInt32(strLine.Split(' ')[0]);
                            var ball = Convert.ToByte(strLine.Split(' ')[1]);
                            balls.Add(new Баллы {Код_абитуриента = id, Баллы1 = ball});
                        }
                        catch (Exception)
                        {
                            balls = null;
                            MessageBox.Show("Неверный формат файла");
                            break;
                        }
                    }

                    if (balls != null)
                    {
                        var subject = dataBase.Предметы.ToList().FirstOrDefault(_ => _.Название_предмета.ToLower() ==
                                                                                     Path.GetFileNameWithoutExtension(subjectName).ToLower());
                        if (subject == null)
                        {
                            var newSubject = new Предметы();
                            newSubject.Название_предмета = Path.GetFileNameWithoutExtension(subjectName);
                            dataBase.Предметы.Add(newSubject);
                            if (!SaveChanges())
                            {
                                return;
                            }
                            balls.ForEach(_ => _.Код_предмета = newSubject.Код_предмета);
                        }
                        else
                        {
                            balls.ForEach(_ => _.Код_предмета = subject.Код_предмета);
                        }
                        dataBase.Баллы.AddRange(balls);
                        SaveChanges();
                    }
                }
            }
        }

        /// <summary>
        /// Событие происходит при нажатии на кнопку сформировать на вкладке "Формирование групп"
        /// </summary>
        private void buttonGenerate_Click(object sender, EventArgs e)
        {
            try
            {
                var selectedSubject = comboBoxSubject.SelectedItem as Subject;

                if (selectedSubject == null)
                {
                    MessageBox.Show("Выберите предмет для формирвоания группы");
                    return;
                }

                var countStudents = Convert.ToInt32(textBoxCountStudents.Text);
                var numberRoom = textBoxNumberRoom.Text;
                var examinationDate = dateTimePickerExaminationDate.Value;

                if (dataBase.Вступит_экзамен.Count(_ => _.Дата_Время == examinationDate && _.Место == numberRoom) != 0)
                {
                    MessageBox.Show("В это время аудитория занята");
                    return;
                }

                var retingsForSelectedSubject = dataBase
                    .Баллы.Where(_ => _.Код_предмета == selectedSubject.Код_предмета).ToList();
                var existExaminationsOnSubject = dataBase
                    .Вступит_экзамен.Where(_ => _.Код_предмета == selectedSubject.Код_предмета).ToList();
                var enrolleesProfiles = dataBase.Анкета_абитуриента.ToList();
                var абитуриентыКоторыеНеСдавалиПредмет = enrolleesProfiles
                    .Where(_ => existExaminationsOnSubject.All(__ => __.Код_абитуриента != _.Код_абитуриента)).ToList();
                var enrolleesProfilesForSelectedSubject = абитуриентыКоторыеНеСдавалиПредмет
                    .Where(_ => retingsForSelectedSubject.All(__ => __.Код_абитуриента != _.Код_абитуриента))
                    .ToList()
                    .OrderBy(__ => __.Дата_регистрации)
                    .Take(countStudents);

                if (!enrolleesProfilesForSelectedSubject.Any())
                {
                    MessageBox.Show("Не найдены студенты для формирования группы");
                    return;
                }
                foreach (var анкетаАбитуриента in enrolleesProfilesForSelectedSubject)
                {
                    dataBase.Вступит_экзамен.Add(new Вступит_экзамен
                                                 {
                                                     Код_абитуриента = анкетаАбитуриента.Код_абитуриента,
                                                     Код_предмета = selectedSubject.Код_предмета,
                                                     Дата_Время = examinationDate,
                                                     Место = numberRoom
                                                 });
                }

                if (SaveChanges())
                {
                    MessageBox.Show("Группа успешно сформирована");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        #region Методы

        /// <summary>
        /// Сохранение изменений в базу данных
        /// </summary>
        /// <returns>Возвращает true, если сохранение в БД прошло успешно</returns>
        private bool SaveChanges()
        {
            try
            {
                dataBase.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        /// <summary>
        /// Получение индекса выделенной строки в гриде
        /// </summary>
        /// <param name="dataGridView">Грид для поиска выделенной строки</param>
        /// <returns>Индекс строки</returns>
        private int GetSelectedRowInDataGridView(DataGridView dataGridView)
        {
            var selectedRows = dataGridView.SelectedRows;
            var firstSelectedRow = selectedRows.Count == 0 ? -1 : selectedRows[0].Index;

            return firstSelectedRow;
        }

        /// <summary>
        /// Получение идентификатора выделеной строки в гриде
        /// </summary>
        /// <param name="dataGridView">Грид для поиска идентификатора</param>
        /// <param name="rowSelected">Выделенная строка в гриде</param>
        /// <returns>Идентификатор выделенной строки. Идентификатор должен идти в первой колонке</returns>
        private int GetIdSelectedInDataGrid(DataGridView dataGridView, int rowSelected)
        {
            if (rowSelected == -1)
            {
                return -1;
            }
            if (dataGridView.Rows[rowSelected].Cells[0].Value is int)
                return (int) dataGridView.Rows[rowSelected].Cells[0].Value;
            if (dataGridView.Rows[rowSelected].Cells[0].Value is byte)
                return (byte) dataGridView.Rows[rowSelected].Cells[0].Value;

            return -1;
        }

        /// <summary>
        /// Получение идентификатора выделенной строки
        /// </summary>
        /// <param name="dataGridView">Грид для поиска выделенной строки</param>
        /// <returns>Идентификатор выделенной строки. Идентификатор должен идти в первой колонке</returns>
        private int GetIdSelectedInDataGrid(DataGridView dataGridView)
        {
            var rowSelected = GetSelectedRowInDataGridView(dataGridView);

            return GetIdSelectedInDataGrid(dataGridView, rowSelected);
        }

        #endregion
    }
}