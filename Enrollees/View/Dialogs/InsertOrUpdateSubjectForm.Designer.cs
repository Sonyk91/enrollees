﻿namespace Enrollees.View.Dialogs
{
    partial class InsertOrUpdateSubjectForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cancel = new System.Windows.Forms.Button();
            this.saveSubject = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.subjectName = new System.Windows.Forms.TextBox();
            this.specialityCheckedListBox = new System.Windows.Forms.CheckedListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cancel
            // 
            this.cancel.Location = new System.Drawing.Point(225, 274);
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(75, 23);
            this.cancel.TabIndex = 0;
            this.cancel.Text = "Отмена";
            this.cancel.UseVisualStyleBackColor = true;
            this.cancel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.cancel_MouseUp);
            // 
            // saveSubject
            // 
            this.saveSubject.Location = new System.Drawing.Point(144, 274);
            this.saveSubject.Name = "saveSubject";
            this.saveSubject.Size = new System.Drawing.Size(75, 23);
            this.saveSubject.TabIndex = 1;
            this.saveSubject.Text = "Сохранить";
            this.saveSubject.UseVisualStyleBackColor = true;
            this.saveSubject.MouseUp += new System.Windows.Forms.MouseEventHandler(this.saveSubject_MouseUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Название:";
            // 
            // subjectName
            // 
            this.subjectName.Location = new System.Drawing.Point(78, 18);
            this.subjectName.Name = "subjectName";
            this.subjectName.Size = new System.Drawing.Size(222, 20);
            this.subjectName.TabIndex = 3;
            // 
            // specialityCheckedListBox
            // 
            this.specialityCheckedListBox.FormattingEnabled = true;
            this.specialityCheckedListBox.Location = new System.Drawing.Point(12, 75);
            this.specialityCheckedListBox.Name = "specialityCheckedListBox";
            this.specialityCheckedListBox.ScrollAlwaysVisible = true;
            this.specialityCheckedListBox.Size = new System.Drawing.Size(288, 184);
            this.specialityCheckedListBox.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Специальности:";
            // 
            // InsertOrUpdateSubjectForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(312, 309);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.specialityCheckedListBox);
            this.Controls.Add(this.subjectName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.saveSubject);
            this.Controls.Add(this.cancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "InsertOrUpdateSubjectForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "InsertOrUpdateSubjectForm";
            this.Load += new System.EventHandler(this.InsertOrUpdateSubjectForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button cancel;
        private System.Windows.Forms.Button saveSubject;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox subjectName;
        private System.Windows.Forms.CheckedListBox specialityCheckedListBox;
        private System.Windows.Forms.Label label2;
    }
}