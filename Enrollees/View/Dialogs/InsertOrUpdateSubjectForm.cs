﻿#region Usings

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Windows.Forms;
using Enrollees.EFModels;
using Enrollees.View.Converters;
using Enrollees.View.Domains;

#endregion

namespace Enrollees.View.Dialogs
{
    public partial class InsertOrUpdateSubjectForm : Form
    {
        public InsertOrUpdateSubjectForm()
        {
            InitializeComponent();
            Предмет = new Предметы();
            Text = "Добавление предмета";
        }

        public Предметы Предмет { get; set; }

        private void cancel_MouseUp(object sender, MouseEventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Событие возникает при нажатии на кнопку сохранить. Происходит сохранение введенных данных о предемете в БД
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveSubject_MouseUp(object sender, MouseEventArgs e)
        {
            var db = PKEntitiesFactory.Instance;

            Предмет.Название_предмета = subjectName.Text;
            var специальности = specialityCheckedListBox.CheckedItems
                                                        .OfType<Speciality>().ToList()
                                                        .ConvertAll(ConvertСпециальностиToDomainSpeciality.ToViewModel);

            ICollection<ValidationResult> errors = new List<ValidationResult>();

            if (DataAnnotationsValidator.TryValidate(Предмет, out errors))
            {
                db.Предметы.Add(Предмет);
                if (SaveChanges(db)) return;


                foreach (var специальность in специальности)
                {
                    db.Предметы_для_специальностей.Add(new Предметы_для_специальностей {код_предмета = Предмет.Код_предмета, код_специальности = специальность.Код_специальности});
                }


                if (SaveChanges(db)) return;
            }
            else
            {
                MessageBox.Show(errors.First().ErrorMessage, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            Close();
        }

        /// <summary>
        /// Сохранение измений в базу данных
        /// </summary>
        /// <param name="db">База данных</param>
        /// <returns>Возвращает успешное сохранение, если true</returns>
        private bool SaveChanges(PKEntities db)
        {
            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Событие возникает при загрузке формы. Заполняется список специальностей
        /// </summary>
        private void InsertOrUpdateSubjectForm_Load(object sender, EventArgs e)
        {
            var db = PKEntitiesFactory.Instance;
            var specialityList = new List<Speciality>();

            specialityList.AddRange(db.Специальности.ToList()
                                      .ConvertAll(ConvertСпециальностиToDomainSpeciality.ToDomain));
            foreach (var специальность in specialityList)
            {
                specialityCheckedListBox.Items.Add(специальность);
            }
        }
    }
}