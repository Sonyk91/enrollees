﻿#region Usings

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using Enrollees.EFModels;

#endregion

namespace Enrollees
{
    public partial class InsertOrUpdatePersonalDataForm : Form
    {
        private readonly PKEntities db = PKEntitiesFactory.Instance;

        private readonly byte[] fixedIV = {126, 20, 142, 69, 209, 217, 190, 69, 247, 251, 123, 137, 158, 198, 204, 250};

        public InsertOrUpdatePersonalDataForm()
        {
            InitializeComponent();
            Text = "Персональные данные";
            ПерсональныеДанные = new Персональные_данные();
        }

        public Персональные_данные ПерсональныеДанные { get; set; }

        /// <summary>
        /// Событие возникает при нажатии на кнопку Сохранить. Шифруются введенные данные пользователем и сохраняются в базу данных
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void сохранить_MouseUp(object sender, MouseEventArgs e)
        {
            var personalDataModel = new Персональные_данные();
            personalDataModel.Адрес_проживания = адресПроживания.Text;
            personalDataModel.Адрес_регистрации = адресРегистрации.Text;
            personalDataModel.Гражданство = гражданство.Text;
            personalDataModel.Кем_выдан = кемВыдан.Text;
            personalDataModel.Когда_выдан = когдаВыдан.Text;
            personalDataModel.Код_подразделения = кодПодразделения.Text;
            personalDataModel.Место_рождения = местоРождения.Text;
            personalDataModel.Номер = номер.Text;
            personalDataModel.Серия = серия.Text;
            personalDataModel.Телефон = телефон.Text;
            personalDataModel.Тип_документа_личность = типДокументаЛичность.Text;
            personalDataModel.Эл_почта = элПочта.Text;

            ICollection<ValidationResult> errors = new List<ValidationResult>();
            if (DataAnnotationsValidator.TryValidate(personalDataModel, out errors))
            {
                // Зашифровать данные.
                var passwordHash = AesExample.GetMd5Hash(Program.Password);
                var key = Encoding.Default.GetBytes(passwordHash);
                ПерсональныеДанные.Адрес_проживания = Convert.ToBase64String(AesExample.EncryptStringToBytes_Aes(адресПроживания.Text, key, fixedIV));

                ПерсональныеДанные.Адрес_регистрации = Convert.ToBase64String(AesExample.EncryptStringToBytes_Aes(адресРегистрации.Text, key, fixedIV));

                ПерсональныеДанные.Гражданство = Convert.ToBase64String(AesExample.EncryptStringToBytes_Aes(гражданство.Text, key, fixedIV));


                ПерсональныеДанные.Кем_выдан = Convert.ToBase64String(AesExample.EncryptStringToBytes_Aes(кемВыдан.Text, key, fixedIV));

                ПерсональныеДанные.Когда_выдан = Convert.ToBase64String(AesExample.EncryptStringToBytes_Aes(когдаВыдан.Text, key, fixedIV));

                ПерсональныеДанные.Код_подразделения = Convert.ToBase64String(AesExample.EncryptStringToBytes_Aes(кодПодразделения.Text, key, fixedIV));

                ПерсональныеДанные.Место_рождения = Convert.ToBase64String(AesExample.EncryptStringToBytes_Aes(местоРождения.Text, key, fixedIV));

                ПерсональныеДанные.Номер = Convert.ToBase64String(AesExample.EncryptStringToBytes_Aes(номер.Text, key, fixedIV));

                ПерсональныеДанные.Серия = Convert.ToBase64String(AesExample.EncryptStringToBytes_Aes(серия.Text, key, fixedIV));

                ПерсональныеДанные.Телефон = Convert.ToBase64String(AesExample.EncryptStringToBytes_Aes(телефон.Text, key, fixedIV));

                ПерсональныеДанные.Тип_документа_личность = Convert.ToBase64String(AesExample.EncryptStringToBytes_Aes(типДокументаЛичность.Text, key, fixedIV));

                ПерсональныеДанные.Эл_почта = Convert.ToBase64String(AesExample.EncryptStringToBytes_Aes(элПочта.Text, key, fixedIV));

                // Обновить данные.
                var personalData = db.Персональные_данные.FirstOrDefault(p => p.Код_абитуриента == ПерсональныеДанные.Код_абитуриента);

                if (personalData != null)
                {
                    personalData.Адрес_проживания = ПерсональныеДанные.Адрес_проживания;
                    personalData.Адрес_регистрации = ПерсональныеДанные.Адрес_регистрации;
                    personalData.Гражданство = ПерсональныеДанные.Гражданство;
                    personalData.Кем_выдан = ПерсональныеДанные.Кем_выдан;
                    personalData.Когда_выдан = ПерсональныеДанные.Когда_выдан;
                    personalData.Код_подразделения = ПерсональныеДанные.Код_подразделения;
                    personalData.Место_рождения = ПерсональныеДанные.Место_рождения;
                    personalData.Номер = ПерсональныеДанные.Номер;
                    personalData.Серия = ПерсональныеДанные.Серия;
                    personalData.Телефон = ПерсональныеДанные.Телефон;
                    personalData.Тип_документа_личность = ПерсональныеДанные.Тип_документа_личность;
                    personalData.Эл_почта = ПерсональныеДанные.Эл_почта;
                }
                else
                {
                    // Добавить новые данные.
                    db.Персональные_данные.Add(ПерсональныеДанные);
                }

                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            else
            {
                MessageBox.Show(errors.First().ErrorMessage, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            Close();
        }

        /// <summary>
        /// Событие возникает при загрузке формы. Дешифруются данные, если они есть и заполняются поля
        /// </summary>
        private void InsertOrUpdatePersonalDataForm_Load(object sender, EventArgs e)
        {
            if (Program.Password == null)
            {
                // Показать окно ввода пароля.
                var passwordRequestForm = new PasswordRequestForm();
                passwordRequestForm.ShowDialog();
            }

            var personalData = db.Персональные_данные.FirstOrDefault(p => p.Код_абитуриента == ПерсональныеДанные.Код_абитуриента);

            if (personalData != null)
            {
                var passwordHash = AesExample.GetMd5Hash(Program.Password);
                var key = Encoding.Default.GetBytes(passwordHash);

                // Расшифровать данные.
                try
                {
                    адресПроживания.Text = AesExample.DecryptStringFromBytes_Aes(Convert.FromBase64String(personalData.Адрес_проживания), key, fixedIV);

                    адресРегистрации.Text = AesExample.DecryptStringFromBytes_Aes(Convert.FromBase64String(personalData.Адрес_регистрации), key, fixedIV);

                    гражданство.Text = AesExample.DecryptStringFromBytes_Aes(Convert.FromBase64String(personalData.Гражданство), key, fixedIV);

                    кемВыдан.Text = AesExample.DecryptStringFromBytes_Aes(Convert.FromBase64String(personalData.Кем_выдан), key, fixedIV);

                    когдаВыдан.Value = DateTime.Parse(AesExample.DecryptStringFromBytes_Aes(Convert.FromBase64String(personalData.Когда_выдан), key, fixedIV));

                    кодПодразделения.Text = AesExample.DecryptStringFromBytes_Aes(Convert.FromBase64String(personalData.Код_подразделения), key, fixedIV);

                    местоРождения.Text = AesExample.DecryptStringFromBytes_Aes(Convert.FromBase64String(personalData.Место_рождения), key, fixedIV);

                    номер.Text = AesExample.DecryptStringFromBytes_Aes(Convert.FromBase64String(personalData.Номер), key, fixedIV);

                    серия.Text = AesExample.DecryptStringFromBytes_Aes(Convert.FromBase64String(personalData.Серия), key, fixedIV);

                    телефон.Text = AesExample.DecryptStringFromBytes_Aes(Convert.FromBase64String(personalData.Телефон), key, fixedIV);

                    типДокументаЛичность.Text = AesExample.DecryptStringFromBytes_Aes(Convert.FromBase64String(personalData.Тип_документа_личность), key, fixedIV);

                    элПочта.Text = AesExample.DecryptStringFromBytes_Aes(Convert.FromBase64String(personalData.Эл_почта), key, fixedIV);
                }
                catch (CryptographicException)
                {
                    Program.Password = null;
                    MessageBox.Show("Неверный пароль");
                    Close();
                }
            }
        }
    }
}