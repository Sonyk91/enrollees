﻿namespace Enrollees
{
    partial class InsertOrUpdatePersonalDataForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.когдаВыдан = new System.Windows.Forms.DateTimePicker();
            this.гражданство = new System.Windows.Forms.TextBox();
            this.типДокументаЛичность = new System.Windows.Forms.TextBox();
            this.серия = new System.Windows.Forms.TextBox();
            this.номер = new System.Windows.Forms.TextBox();
            this.кемВыдан = new System.Windows.Forms.TextBox();
            this.кодПодразделения = new System.Windows.Forms.TextBox();
            this.местоРождения = new System.Windows.Forms.TextBox();
            this.адресРегистрации = new System.Windows.Forms.TextBox();
            this.адресПроживания = new System.Windows.Forms.TextBox();
            this.телефон = new System.Windows.Forms.TextBox();
            this.элПочта = new System.Windows.Forms.TextBox();
            this.сохранить = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.label10, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.label11, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.label12, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.когдаВыдан, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.гражданство, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.типДокументаЛичность, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.серия, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.номер, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.кемВыдан, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.кодПодразделения, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.местоРождения, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.адресРегистрации, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.адресПроживания, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.телефон, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.элПочта, 1, 11);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 6);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 12;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333332F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(512, 330);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 27);
            this.label1.TabIndex = 0;
            this.label1.Text = "Гражданство:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(3, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(122, 27);
            this.label2.TabIndex = 1;
            this.label2.Text = "Тип документа:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(3, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 27);
            this.label3.TabIndex = 2;
            this.label3.Text = "Серия:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(3, 81);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(122, 27);
            this.label4.TabIndex = 3;
            this.label4.Text = "Номер:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(3, 108);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(122, 27);
            this.label5.TabIndex = 4;
            this.label5.Text = "Кем выдан:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(3, 135);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(122, 27);
            this.label6.TabIndex = 5;
            this.label6.Text = "Когда выдан:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Location = new System.Drawing.Point(3, 162);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(122, 27);
            this.label7.TabIndex = 6;
            this.label7.Text = "Код подразделения:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Location = new System.Drawing.Point(3, 189);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(122, 27);
            this.label8.TabIndex = 7;
            this.label8.Text = "Место рождения:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Location = new System.Drawing.Point(3, 216);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(122, 27);
            this.label9.TabIndex = 8;
            this.label9.Text = "Адрес регистрации:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Location = new System.Drawing.Point(3, 243);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(122, 27);
            this.label10.TabIndex = 9;
            this.label10.Text = "Адрес проживания:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Location = new System.Drawing.Point(3, 270);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(122, 27);
            this.label11.TabIndex = 10;
            this.label11.Text = "Телефон:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Location = new System.Drawing.Point(3, 297);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(122, 33);
            this.label12.TabIndex = 11;
            this.label12.Text = "Эл почта:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // когдаВыдан
            // 
            this.когдаВыдан.Dock = System.Windows.Forms.DockStyle.Fill;
            this.когдаВыдан.Location = new System.Drawing.Point(131, 138);
            this.когдаВыдан.Name = "когдаВыдан";
            this.когдаВыдан.Size = new System.Drawing.Size(378, 20);
            this.когдаВыдан.TabIndex = 12;
            // 
            // гражданство
            // 
            this.гражданство.Dock = System.Windows.Forms.DockStyle.Fill;
            this.гражданство.Location = new System.Drawing.Point(131, 3);
            this.гражданство.Name = "гражданство";
            this.гражданство.Size = new System.Drawing.Size(378, 20);
            this.гражданство.TabIndex = 13;
            // 
            // типДокументаЛичность
            // 
            this.типДокументаЛичность.Dock = System.Windows.Forms.DockStyle.Fill;
            this.типДокументаЛичность.Location = new System.Drawing.Point(131, 30);
            this.типДокументаЛичность.Name = "типДокументаЛичность";
            this.типДокументаЛичность.Size = new System.Drawing.Size(378, 20);
            this.типДокументаЛичность.TabIndex = 14;
            // 
            // серия
            // 
            this.серия.Dock = System.Windows.Forms.DockStyle.Fill;
            this.серия.Location = new System.Drawing.Point(131, 57);
            this.серия.Name = "серия";
            this.серия.Size = new System.Drawing.Size(378, 20);
            this.серия.TabIndex = 15;
            // 
            // номер
            // 
            this.номер.Dock = System.Windows.Forms.DockStyle.Fill;
            this.номер.Location = new System.Drawing.Point(131, 84);
            this.номер.Name = "номер";
            this.номер.Size = new System.Drawing.Size(378, 20);
            this.номер.TabIndex = 16;
            // 
            // кемВыдан
            // 
            this.кемВыдан.Dock = System.Windows.Forms.DockStyle.Fill;
            this.кемВыдан.Location = new System.Drawing.Point(131, 111);
            this.кемВыдан.Name = "кемВыдан";
            this.кемВыдан.Size = new System.Drawing.Size(378, 20);
            this.кемВыдан.TabIndex = 17;
            // 
            // кодПодразделения
            // 
            this.кодПодразделения.Dock = System.Windows.Forms.DockStyle.Fill;
            this.кодПодразделения.Location = new System.Drawing.Point(131, 165);
            this.кодПодразделения.Name = "кодПодразделения";
            this.кодПодразделения.Size = new System.Drawing.Size(378, 20);
            this.кодПодразделения.TabIndex = 18;
            // 
            // местоРождения
            // 
            this.местоРождения.Dock = System.Windows.Forms.DockStyle.Fill;
            this.местоРождения.Location = new System.Drawing.Point(131, 192);
            this.местоРождения.Name = "местоРождения";
            this.местоРождения.Size = new System.Drawing.Size(378, 20);
            this.местоРождения.TabIndex = 19;
            // 
            // адресРегистрации
            // 
            this.адресРегистрации.Dock = System.Windows.Forms.DockStyle.Fill;
            this.адресРегистрации.Location = new System.Drawing.Point(131, 219);
            this.адресРегистрации.Name = "адресРегистрации";
            this.адресРегистрации.Size = new System.Drawing.Size(378, 20);
            this.адресРегистрации.TabIndex = 20;
            // 
            // адресПроживания
            // 
            this.адресПроживания.Dock = System.Windows.Forms.DockStyle.Fill;
            this.адресПроживания.Location = new System.Drawing.Point(131, 246);
            this.адресПроживания.Name = "адресПроживания";
            this.адресПроживания.Size = new System.Drawing.Size(378, 20);
            this.адресПроживания.TabIndex = 21;
            // 
            // телефон
            // 
            this.телефон.Dock = System.Windows.Forms.DockStyle.Fill;
            this.телефон.Location = new System.Drawing.Point(131, 273);
            this.телефон.Name = "телефон";
            this.телефон.Size = new System.Drawing.Size(378, 20);
            this.телефон.TabIndex = 22;
            // 
            // элПочта
            // 
            this.элПочта.Dock = System.Windows.Forms.DockStyle.Fill;
            this.элПочта.Location = new System.Drawing.Point(131, 300);
            this.элПочта.Name = "элПочта";
            this.элПочта.Size = new System.Drawing.Size(378, 20);
            this.элПочта.TabIndex = 23;
            // 
            // сохранить
            // 
            this.сохранить.Location = new System.Drawing.Point(434, 335);
            this.сохранить.Name = "сохранить";
            this.сохранить.Size = new System.Drawing.Size(75, 23);
            this.сохранить.TabIndex = 1;
            this.сохранить.Text = "Сохранить";
            this.сохранить.UseVisualStyleBackColor = true;
            this.сохранить.MouseUp += new System.Windows.Forms.MouseEventHandler(this.сохранить_MouseUp);
            // 
            // InsertOrUpdatePersonalDataForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(519, 367);
            this.Controls.Add(this.сохранить);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "InsertOrUpdatePersonalDataForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "InsertOrUpdatePersonalDataForm";
            this.Load += new System.EventHandler(this.InsertOrUpdatePersonalDataForm_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DateTimePicker когдаВыдан;
        private System.Windows.Forms.TextBox гражданство;
        private System.Windows.Forms.TextBox типДокументаЛичность;
        private System.Windows.Forms.TextBox серия;
        private System.Windows.Forms.TextBox номер;
        private System.Windows.Forms.TextBox кемВыдан;
        private System.Windows.Forms.TextBox кодПодразделения;
        private System.Windows.Forms.TextBox местоРождения;
        private System.Windows.Forms.TextBox адресРегистрации;
        private System.Windows.Forms.TextBox адресПроживания;
        private System.Windows.Forms.TextBox телефон;
        private System.Windows.Forms.TextBox элПочта;
        private System.Windows.Forms.Button сохранить;
    }
}