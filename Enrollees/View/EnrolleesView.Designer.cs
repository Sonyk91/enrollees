﻿namespace Enrollees.View
{
    partial class EnrolleesView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.загрузитьОценкиПоПредметуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControlGroupFormation = new System.Windows.Forms.TabControl();
            this.worksheetTabPage = new System.Windows.Forms.TabPage();
            this.buttonDeleteSelectedEnrollee = new System.Windows.Forms.Button();
            this.buttonUpdateSelectEnrollee = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbFamily = new System.Windows.Forms.TextBox();
            this.tbName = new System.Windows.Forms.TextBox();
            this.tbLastName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.датаРегистрации = new System.Windows.Forms.DateTimePicker();
            this.датаРождения = new System.Windows.Forms.DateTimePicker();
            this.panel1 = new System.Windows.Forms.Panel();
            this.полЖ = new System.Windows.Forms.RadioButton();
            this.полМ = new System.Windows.Forms.RadioButton();
            this.worksheetDataGridView = new System.Windows.Forms.DataGridView();
            this.Код_абитуриента = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Дата_регистрации = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Фамилия = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Имя = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Отчество = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Пол = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Дата_рождения = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.personalData = new System.Windows.Forms.DataGridViewButtonColumn();
            this.tabSubjects = new System.Windows.Forms.TabPage();
            this.label7 = new System.Windows.Forms.Label();
            this.specialityCheckedListBox = new System.Windows.Forms.CheckedListBox();
            this.subjectName = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.subjectsDataGridView = new System.Windows.Forms.DataGridView();
            this.Код = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Название = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.facultyName = new System.Windows.Forms.TextBox();
            this.specialityName = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.specialityDataGridView = new System.Windows.Forms.DataGridView();
            this.Код_специальности = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Название_специальности = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Название_факультета = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxCountStudents = new System.Windows.Forms.TextBox();
            this.textBoxNumberRoom = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.dateTimePickerExaminationDate = new System.Windows.Forms.DateTimePicker();
            this.label13 = new System.Windows.Forms.Label();
            this.comboBoxSubject = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.buttonGenerate = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.tabControlGroupFormation.SuspendLayout();
            this.worksheetTabPage.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.worksheetDataGridView)).BeginInit();
            this.tabSubjects.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.subjectsDataGridView)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.specialityDataGridView)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(852, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.загрузитьОценкиПоПредметуToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(48, 20);
            this.toolStripMenuItem1.Text = "Файл";
            // 
            // загрузитьОценкиПоПредметуToolStripMenuItem
            // 
            this.загрузитьОценкиПоПредметуToolStripMenuItem.Name = "загрузитьОценкиПоПредметуToolStripMenuItem";
            this.загрузитьОценкиПоПредметуToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.загрузитьОценкиПоПредметуToolStripMenuItem.Text = "Загрузить оценки по предмету";
            this.загрузитьОценкиПоПредметуToolStripMenuItem.Click += new System.EventHandler(this.загрузитьОценкиПоПредметуToolStripMenuItem_Click);
            // 
            // tabControlGroupFormation
            // 
            this.tabControlGroupFormation.Controls.Add(this.worksheetTabPage);
            this.tabControlGroupFormation.Controls.Add(this.tabSubjects);
            this.tabControlGroupFormation.Controls.Add(this.tabPage3);
            this.tabControlGroupFormation.Controls.Add(this.tabPage1);
            this.tabControlGroupFormation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlGroupFormation.Location = new System.Drawing.Point(0, 24);
            this.tabControlGroupFormation.Name = "tabControlGroupFormation";
            this.tabControlGroupFormation.SelectedIndex = 0;
            this.tabControlGroupFormation.Size = new System.Drawing.Size(852, 483);
            this.tabControlGroupFormation.TabIndex = 1;
            this.tabControlGroupFormation.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControl1_Selected);
            // 
            // worksheetTabPage
            // 
            this.worksheetTabPage.Controls.Add(this.buttonDeleteSelectedEnrollee);
            this.worksheetTabPage.Controls.Add(this.buttonUpdateSelectEnrollee);
            this.worksheetTabPage.Controls.Add(this.button1);
            this.worksheetTabPage.Controls.Add(this.tableLayoutPanel1);
            this.worksheetTabPage.Controls.Add(this.worksheetDataGridView);
            this.worksheetTabPage.Location = new System.Drawing.Point(4, 22);
            this.worksheetTabPage.Name = "worksheetTabPage";
            this.worksheetTabPage.Size = new System.Drawing.Size(844, 457);
            this.worksheetTabPage.TabIndex = 0;
            this.worksheetTabPage.Text = "Абитуриенты";
            this.worksheetTabPage.UseVisualStyleBackColor = true;
            // 
            // buttonDeleteSelectedEnrollee
            // 
            this.buttonDeleteSelectedEnrollee.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonDeleteSelectedEnrollee.Location = new System.Drawing.Point(661, 355);
            this.buttonDeleteSelectedEnrollee.Name = "buttonDeleteSelectedEnrollee";
            this.buttonDeleteSelectedEnrollee.Size = new System.Drawing.Size(180, 36);
            this.buttonDeleteSelectedEnrollee.TabIndex = 8;
            this.buttonDeleteSelectedEnrollee.Text = "Удалить";
            this.buttonDeleteSelectedEnrollee.UseVisualStyleBackColor = true;
            this.buttonDeleteSelectedEnrollee.Click += new System.EventHandler(this.buttonDeleteSelectedEnrollee_Click);
            // 
            // buttonUpdateSelectEnrollee
            // 
            this.buttonUpdateSelectEnrollee.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonUpdateSelectEnrollee.Location = new System.Drawing.Point(661, 316);
            this.buttonUpdateSelectEnrollee.Name = "buttonUpdateSelectEnrollee";
            this.buttonUpdateSelectEnrollee.Size = new System.Drawing.Size(180, 36);
            this.buttonUpdateSelectEnrollee.TabIndex = 7;
            this.buttonUpdateSelectEnrollee.Text = "Обновить";
            this.buttonUpdateSelectEnrollee.UseVisualStyleBackColor = true;
            this.buttonUpdateSelectEnrollee.Click += new System.EventHandler(this.buttonUpdateSelectedEnrollee_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(661, 277);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(180, 36);
            this.button1.TabIndex = 5;
            this.button1.Text = "Добавить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.buttonAddEnrollee_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tbFamily, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tbName, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.tbLastName, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.датаРегистрации, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.датаРождения, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 5);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(8, 277);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(650, 159);
            this.tableLayoutPanel1.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Left;
            this.label6.Location = new System.Drawing.Point(3, 130);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 29);
            this.label6.TabIndex = 10;
            this.label6.Text = "Пол:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "Фамилия:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbFamily
            // 
            this.tbFamily.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbFamily.Location = new System.Drawing.Point(112, 3);
            this.tbFamily.Name = "tbFamily";
            this.tbFamily.Size = new System.Drawing.Size(535, 20);
            this.tbFamily.TabIndex = 1;
            // 
            // tbName
            // 
            this.tbName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbName.Location = new System.Drawing.Point(112, 29);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(535, 20);
            this.tbName.TabIndex = 2;
            // 
            // tbLastName
            // 
            this.tbLastName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbLastName.Location = new System.Drawing.Point(112, 55);
            this.tbLastName.Name = "tbLastName";
            this.tbLastName.Size = new System.Drawing.Size(535, 20);
            this.tbLastName.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Left;
            this.label2.Location = new System.Drawing.Point(3, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 26);
            this.label2.TabIndex = 6;
            this.label2.Text = "Имя:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Left;
            this.label3.Location = new System.Drawing.Point(3, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 26);
            this.label3.TabIndex = 7;
            this.label3.Text = "Отчество:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Left;
            this.label4.Location = new System.Drawing.Point(3, 78);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(103, 26);
            this.label4.TabIndex = 8;
            this.label4.Text = "Дата регистрации:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Left;
            this.label5.Location = new System.Drawing.Point(3, 104);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 26);
            this.label5.TabIndex = 9;
            this.label5.Text = "Дата рождения:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // датаРегистрации
            // 
            this.датаРегистрации.Dock = System.Windows.Forms.DockStyle.Fill;
            this.датаРегистрации.Location = new System.Drawing.Point(112, 81);
            this.датаРегистрации.Name = "датаРегистрации";
            this.датаРегистрации.Size = new System.Drawing.Size(535, 20);
            this.датаРегистрации.TabIndex = 12;
            // 
            // датаРождения
            // 
            this.датаРождения.Dock = System.Windows.Forms.DockStyle.Fill;
            this.датаРождения.Location = new System.Drawing.Point(112, 107);
            this.датаРождения.Name = "датаРождения";
            this.датаРождения.Size = new System.Drawing.Size(535, 20);
            this.датаРождения.TabIndex = 13;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.полЖ);
            this.panel1.Controls.Add(this.полМ);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(112, 133);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(535, 23);
            this.panel1.TabIndex = 14;
            // 
            // полЖ
            // 
            this.полЖ.AutoSize = true;
            this.полЖ.Location = new System.Drawing.Point(34, 3);
            this.полЖ.Name = "полЖ";
            this.полЖ.Size = new System.Drawing.Size(36, 17);
            this.полЖ.TabIndex = 0;
            this.полЖ.TabStop = true;
            this.полЖ.Text = "Ж";
            this.полЖ.UseVisualStyleBackColor = true;
            // 
            // полМ
            // 
            this.полМ.AutoSize = true;
            this.полМ.Location = new System.Drawing.Point(3, 3);
            this.полМ.Name = "полМ";
            this.полМ.Size = new System.Drawing.Size(34, 17);
            this.полМ.TabIndex = 0;
            this.полМ.TabStop = true;
            this.полМ.Text = "М";
            this.полМ.UseVisualStyleBackColor = true;
            // 
            // worksheetDataGridView
            // 
            this.worksheetDataGridView.AllowUserToAddRows = false;
            this.worksheetDataGridView.AllowUserToDeleteRows = false;
            this.worksheetDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.worksheetDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.worksheetDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Код_абитуриента,
            this.Дата_регистрации,
            this.Фамилия,
            this.Имя,
            this.Отчество,
            this.Пол,
            this.Дата_рождения,
            this.personalData});
            this.worksheetDataGridView.Location = new System.Drawing.Point(0, 3);
            this.worksheetDataGridView.MultiSelect = false;
            this.worksheetDataGridView.Name = "worksheetDataGridView";
            this.worksheetDataGridView.ReadOnly = true;
            this.worksheetDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.worksheetDataGridView.Size = new System.Drawing.Size(844, 267);
            this.worksheetDataGridView.TabIndex = 4;
            this.worksheetDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.worksheetDataGridView_CellContentClick);
            // 
            // Код_абитуриента
            // 
            this.Код_абитуриента.HeaderText = "Код абитуриента";
            this.Код_абитуриента.Name = "Код_абитуриента";
            this.Код_абитуриента.ReadOnly = true;
            // 
            // Дата_регистрации
            // 
            this.Дата_регистрации.HeaderText = "Дата регистрации";
            this.Дата_регистрации.Name = "Дата_регистрации";
            this.Дата_регистрации.ReadOnly = true;
            // 
            // Фамилия
            // 
            this.Фамилия.HeaderText = "Фамилия";
            this.Фамилия.Name = "Фамилия";
            this.Фамилия.ReadOnly = true;
            // 
            // Имя
            // 
            this.Имя.HeaderText = "Имя";
            this.Имя.Name = "Имя";
            this.Имя.ReadOnly = true;
            // 
            // Отчество
            // 
            this.Отчество.HeaderText = "Отчество";
            this.Отчество.Name = "Отчество";
            this.Отчество.ReadOnly = true;
            // 
            // Пол
            // 
            this.Пол.HeaderText = "Пол";
            this.Пол.Name = "Пол";
            this.Пол.ReadOnly = true;
            // 
            // Дата_рождения
            // 
            this.Дата_рождения.HeaderText = "Дата рождения";
            this.Дата_рождения.Name = "Дата_рождения";
            this.Дата_рождения.ReadOnly = true;
            // 
            // personalData
            // 
            this.personalData.HeaderText = "Пресональные данные";
            this.personalData.Name = "personalData";
            this.personalData.ReadOnly = true;
            // 
            // tabSubjects
            // 
            this.tabSubjects.Controls.Add(this.label7);
            this.tabSubjects.Controls.Add(this.specialityCheckedListBox);
            this.tabSubjects.Controls.Add(this.subjectName);
            this.tabSubjects.Controls.Add(this.label8);
            this.tabSubjects.Controls.Add(this.button2);
            this.tabSubjects.Controls.Add(this.button3);
            this.tabSubjects.Controls.Add(this.button4);
            this.tabSubjects.Controls.Add(this.subjectsDataGridView);
            this.tabSubjects.Location = new System.Drawing.Point(4, 22);
            this.tabSubjects.Name = "tabSubjects";
            this.tabSubjects.Size = new System.Drawing.Size(844, 457);
            this.tabSubjects.TabIndex = 1;
            this.tabSubjects.Text = "Предметы";
            this.tabSubjects.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 304);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Специальности:";
            // 
            // specialityCheckedListBox
            // 
            this.specialityCheckedListBox.Enabled = false;
            this.specialityCheckedListBox.FormattingEnabled = true;
            this.specialityCheckedListBox.Location = new System.Drawing.Point(8, 320);
            this.specialityCheckedListBox.Name = "specialityCheckedListBox";
            this.specialityCheckedListBox.ScrollAlwaysVisible = true;
            this.specialityCheckedListBox.Size = new System.Drawing.Size(412, 124);
            this.specialityCheckedListBox.TabIndex = 14;
            // 
            // subjectName
            // 
            this.subjectName.Location = new System.Drawing.Point(98, 279);
            this.subjectName.Name = "subjectName";
            this.subjectName.Size = new System.Drawing.Size(322, 20);
            this.subjectName.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 282);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Название:";
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(661, 355);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(180, 36);
            this.button2.TabIndex = 11;
            this.button2.Text = "Удалить";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.buttonDeleteSelectedSubject_Click);
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.Location = new System.Drawing.Point(661, 316);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(180, 36);
            this.button3.TabIndex = 10;
            this.button3.Text = "Обновить";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.buttonUpdateSelectedSubject_Click);
            // 
            // button4
            // 
            this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button4.Location = new System.Drawing.Point(661, 277);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(180, 36);
            this.button4.TabIndex = 9;
            this.button4.Text = "Добавить";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.buttonAddSubject_Click);
            // 
            // subjectsDataGridView
            // 
            this.subjectsDataGridView.AllowUserToAddRows = false;
            this.subjectsDataGridView.AllowUserToDeleteRows = false;
            this.subjectsDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.subjectsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.subjectsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Код,
            this.Название});
            this.subjectsDataGridView.Location = new System.Drawing.Point(0, 3);
            this.subjectsDataGridView.MultiSelect = false;
            this.subjectsDataGridView.Name = "subjectsDataGridView";
            this.subjectsDataGridView.ReadOnly = true;
            this.subjectsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.subjectsDataGridView.Size = new System.Drawing.Size(844, 267);
            this.subjectsDataGridView.TabIndex = 1;
            this.subjectsDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.subjectsDataGridView_CellClick);
            // 
            // Код
            // 
            this.Код.HeaderText = "Код";
            this.Код.Name = "Код";
            this.Код.ReadOnly = true;
            // 
            // Название
            // 
            this.Название.HeaderText = "Название";
            this.Название.Name = "Название";
            this.Название.ReadOnly = true;
            this.Название.Width = 200;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.facultyName);
            this.tabPage3.Controls.Add(this.specialityName);
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Controls.Add(this.label10);
            this.tabPage3.Controls.Add(this.button5);
            this.tabPage3.Controls.Add(this.button6);
            this.tabPage3.Controls.Add(this.button7);
            this.tabPage3.Controls.Add(this.specialityDataGridView);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(844, 457);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Специальности";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // facultyName
            // 
            this.facultyName.Location = new System.Drawing.Point(100, 316);
            this.facultyName.Name = "facultyName";
            this.facultyName.Size = new System.Drawing.Size(273, 20);
            this.facultyName.TabIndex = 19;
            // 
            // specialityName
            // 
            this.specialityName.Location = new System.Drawing.Point(100, 277);
            this.specialityName.Name = "specialityName";
            this.specialityName.Size = new System.Drawing.Size(273, 20);
            this.specialityName.TabIndex = 18;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 319);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(66, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Факультет:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 280);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(88, 13);
            this.label10.TabIndex = 16;
            this.label10.Text = "Специальность:";
            // 
            // button5
            // 
            this.button5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button5.Location = new System.Drawing.Point(661, 355);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(180, 36);
            this.button5.TabIndex = 15;
            this.button5.Text = "Удалить";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.buttonDeleteSelectedSpeciality_Click);
            // 
            // button6
            // 
            this.button6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button6.Location = new System.Drawing.Point(661, 316);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(180, 36);
            this.button6.TabIndex = 14;
            this.button6.Text = "Обновить";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.buttonUpdateSelectedSpeciality_Click);
            // 
            // button7
            // 
            this.button7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button7.Location = new System.Drawing.Point(661, 277);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(180, 36);
            this.button7.TabIndex = 13;
            this.button7.Text = "Добавить";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.buttonAddSpeciality_Click);
            // 
            // specialityDataGridView
            // 
            this.specialityDataGridView.AllowUserToAddRows = false;
            this.specialityDataGridView.AllowUserToDeleteRows = false;
            this.specialityDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.specialityDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Код_специальности,
            this.Название_специальности,
            this.Название_факультета});
            this.specialityDataGridView.Location = new System.Drawing.Point(0, 3);
            this.specialityDataGridView.MultiSelect = false;
            this.specialityDataGridView.Name = "specialityDataGridView";
            this.specialityDataGridView.ReadOnly = true;
            this.specialityDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.specialityDataGridView.Size = new System.Drawing.Size(844, 267);
            this.specialityDataGridView.TabIndex = 12;
            this.specialityDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.specialityDataGridView_CellClick);
            // 
            // Код_специальности
            // 
            this.Код_специальности.HeaderText = "Код специальности";
            this.Код_специальности.Name = "Код_специальности";
            this.Код_специальности.ReadOnly = true;
            // 
            // Название_специальности
            // 
            this.Название_специальности.HeaderText = "Название специальности";
            this.Название_специальности.Name = "Название_специальности";
            this.Название_специальности.ReadOnly = true;
            this.Название_специальности.Width = 200;
            // 
            // Название_факультета
            // 
            this.Название_факультета.HeaderText = "Название факультета";
            this.Название_факультета.Name = "Название_факультета";
            this.Название_факультета.ReadOnly = true;
            this.Название_факультета.Width = 200;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.buttonGenerate);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.comboBoxSubject);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.dateTimePickerExaminationDate);
            this.tabPage1.Controls.Add(this.textBoxNumberRoom);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.textBoxCountStudents);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(844, 457);
            this.tabPage1.TabIndex = 3;
            this.tabPage1.Text = "Формирование групп";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 40);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(95, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Кол-во студентов";
            // 
            // textBoxCountStudents
            // 
            this.textBoxCountStudents.Location = new System.Drawing.Point(124, 38);
            this.textBoxCountStudents.Name = "textBoxCountStudents";
            this.textBoxCountStudents.Size = new System.Drawing.Size(221, 20);
            this.textBoxCountStudents.TabIndex = 1;
            this.textBoxCountStudents.Text = "20";
            // 
            // textBoxNumberRoom
            // 
            this.textBoxNumberRoom.Location = new System.Drawing.Point(124, 64);
            this.textBoxNumberRoom.Name = "textBoxNumberRoom";
            this.textBoxNumberRoom.Size = new System.Drawing.Size(221, 20);
            this.textBoxNumberRoom.TabIndex = 3;
            this.textBoxNumberRoom.Text = "404";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 68);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(96, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "Номер аудитории";
            // 
            // dateTimePickerExaminationDate
            // 
            this.dateTimePickerExaminationDate.CustomFormat = "MM.dd.yyyy HH:mm:ss";
            this.dateTimePickerExaminationDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerExaminationDate.Location = new System.Drawing.Point(124, 90);
            this.dateTimePickerExaminationDate.Name = "dateTimePickerExaminationDate";
            this.dateTimePickerExaminationDate.Size = new System.Drawing.Size(221, 20);
            this.dateTimePickerExaminationDate.TabIndex = 4;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(9, 96);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(76, 13);
            this.label13.TabIndex = 5;
            this.label13.Text = "Назначить на";
            // 
            // comboBoxSubject
            // 
            this.comboBoxSubject.FormattingEnabled = true;
            this.comboBoxSubject.Location = new System.Drawing.Point(124, 11);
            this.comboBoxSubject.Name = "comboBoxSubject";
            this.comboBoxSubject.Size = new System.Drawing.Size(221, 21);
            this.comboBoxSubject.TabIndex = 6;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(9, 14);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(52, 13);
            this.label14.TabIndex = 7;
            this.label14.Text = "Предмет";
            // 
            // buttonGenerate
            // 
            this.buttonGenerate.Location = new System.Drawing.Point(12, 116);
            this.buttonGenerate.Name = "buttonGenerate";
            this.buttonGenerate.Size = new System.Drawing.Size(333, 24);
            this.buttonGenerate.TabIndex = 8;
            this.buttonGenerate.Text = "Сформировать";
            this.buttonGenerate.UseVisualStyleBackColor = true;
            this.buttonGenerate.Click += new System.EventHandler(this.buttonGenerate_Click);
            // 
            // EnrolleesView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(852, 507);
            this.Controls.Add(this.tabControlGroupFormation);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "EnrolleesView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Абитуриенты";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControlGroupFormation.ResumeLayout(false);
            this.worksheetTabPage.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.worksheetDataGridView)).EndInit();
            this.tabSubjects.ResumeLayout(false);
            this.tabSubjects.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.subjectsDataGridView)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.specialityDataGridView)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.TabControl tabControlGroupFormation;
        private System.Windows.Forms.TabPage tabSubjects;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage worksheetTabPage;
        private System.Windows.Forms.Button buttonUpdateSelectEnrollee;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbFamily;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.TextBox tbLastName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker датаРегистрации;
        private System.Windows.Forms.DateTimePicker датаРождения;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton полЖ;
        private System.Windows.Forms.RadioButton полМ;
        private System.Windows.Forms.DataGridView worksheetDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn Код_абитуриента;
        private System.Windows.Forms.DataGridViewTextBoxColumn Дата_регистрации;
        private System.Windows.Forms.DataGridViewTextBoxColumn Фамилия;
        private System.Windows.Forms.DataGridViewTextBoxColumn Имя;
        private System.Windows.Forms.DataGridViewTextBoxColumn Отчество;
        private System.Windows.Forms.DataGridViewTextBoxColumn Пол;
        private System.Windows.Forms.DataGridViewTextBoxColumn Дата_рождения;
        private System.Windows.Forms.DataGridViewButtonColumn personalData;
        private System.Windows.Forms.Button buttonDeleteSelectedEnrollee;
        private System.Windows.Forms.DataGridView subjectsDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn Код;
        private System.Windows.Forms.DataGridViewTextBoxColumn Название;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckedListBox specialityCheckedListBox;
        private System.Windows.Forms.TextBox subjectName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.DataGridView specialityDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn Код_специальности;
        private System.Windows.Forms.DataGridViewTextBoxColumn Название_специальности;
        private System.Windows.Forms.DataGridViewTextBoxColumn Название_факультета;
        private System.Windows.Forms.TextBox facultyName;
        private System.Windows.Forms.TextBox specialityName;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ToolStripMenuItem загрузитьОценкиПоПредметуToolStripMenuItem;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button buttonGenerate;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox comboBoxSubject;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DateTimePicker dateTimePickerExaminationDate;
        private System.Windows.Forms.TextBox textBoxNumberRoom;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBoxCountStudents;
        private System.Windows.Forms.Label label11;
    }
}

