﻿#region Usings

using Enrollees.EFModels;

#endregion

namespace Enrollees
{
    public class PKEntitiesFactory
    {
        private static PKEntities context;

        private PKEntitiesFactory()
        {
        }

        public static PKEntities Instance
        {
            get
            {
                if (context == null)
                {
                    context = new PKEntities();
                }

                return context;
            }
        }
    }
}